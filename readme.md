# URL Response Code Checker

This Python script reads a file called `urls.txt`, makes a request to each URL listed in the file, and writes the response code for each URL to a CSV file called `urls_and_responses.csv`.

## Requirements

This script requires the following modules to be installed:

- requests

You can install these modules using pip. For example:

`pip install requests`

## Usage

1. Create a file called `urls.txt` in the same directory as the script.
2. Add each URL you want to check on a separate line in `urls.txt`.
3. Run the script using Python:

`python url_response_code_checker.py`

The script will create a file called `urls_and_responses.csv` in the same directory as the script, which will contain the response code for each URL in `urls.txt`.



