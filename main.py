import requests
import csv

# Set up the headers to include with each request
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Cache-Control': 'max-age=0',
}

# Read the urls.txt file
with open('urls.txt', 'r') as f:
    urls = [line.strip() for line in f]

# Make a request for each URL and write the results to urls_and_responses.csv
with open('urls_and_responses.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['urls', 'responses'])

    for url in urls:
        response = requests.get(url, headers=headers)
        writer.writerow([url, response.status_code])

